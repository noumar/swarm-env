//! Environment setup for containers in Swarm cluster

use std::env::*;
use std::net::*;
use std::path::Path;

fn main() {
    let host_name: String = var("HOSTNAME").expect("$HOSTNAME not defined!");
    let service_name: String = var("SERVICE").expect("$SERVICE not defined!");
    let all: Vec<String> = format!("tasks.{}:80", service_name)
        .to_socket_addrs()
        .expect("Looking up peer IP(s) failed!")
        .filter(|x| x.is_ipv4())
        .map(|x| x.ip().to_string())
        .collect();
    let we: Ipv4Addr = match (host_name.as_str(), 0)
              .to_socket_addrs()
              .expect("Looking up our IP failed!")
              .filter(|x| x.is_ipv4())
              .collect::<Vec<_>>()
              .first()
              .expect("No IP found!")
              .ip() {
        IpAddr::V4(x) => x,
        _ => panic!("Not IPv4!"),
    };
    let service: Ipv4Addr = match (service_name.as_str(), 0)
              .to_socket_addrs()
              .expect("Looking up service IP failed!")
              .filter(|x| x.is_ipv4())
              .collect::<Vec<_>>()
              .first()
              .expect("No IP found!")
              .ip() {
        IpAddr::V4(x) => x,
        _ => panic!("Not IPv4!"),
    };
    let peers: Vec<String> = all.iter()
        .filter(|x| **x != we.to_string())
        .map(|x| x.to_string())
        .collect();

    git2::build::RepoBuilder::new()
        .branch("nextcloud19")
        .clone("https://github.com/EUDAT-B2DROP/b2drop-theme",
               Path::new("/var/www/html/themes/b2drop"))
        .expect("Failed to clone repo!");

    git2::build::RepoBuilder::new()
        .branch("nextcloud19")
        .clone("https://github.com/EUDAT-B2DROP/b2sharebridge.git",
               Path::new("/var/www/html/custom_apps/b2sharebridge"))
        .expect("Failed to clone repo!");

    git2::build::RepoBuilder::new()
        .branch("master")
        .clone("https://gitlab.com/noumar/user_saml.git",
               Path::new("/var/www/html/custom_apps/user_saml"))
        .expect("Failed to clone repo!");

    println!("ALL={}", all.join(","));
    println!("PEERS={}", peers.join(","));
    println!("US={}", we);
    println!("RANK={}", u32::from(we) - u32::from(service));
    println!("MASTER={}", Ipv4Addr::from(u32::from(service) + 1));
}
